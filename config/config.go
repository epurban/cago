package config // import "electric-it.io/cago/config"

import (
	"os"
	"path/filepath"

	"github.com/apex/log"
	"github.com/cavaliercoder/grab"
	"github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

const (
	// LocalManuallyCreatedConfigFilePath is the file that users can create to override all other configuration files
	LocalManuallyCreatedConfigFilePath = ".cago/cago.yaml"

	// LocalCachedConfigFilePath is the file that will be used to cache the last downloaded remote configuration file
	LocalCachedConfigFilePath = ".cago/cached.cago.yaml"

	RemoteConfigURLEnvironmentVariable = "CAGO_CONFIG_URL"
)

// LoadConfigurationFile loads the config file in the following precedence: CAGO_CONFIG_URL env variable, user home directory
func LoadConfigurationFile(configFileFlagValue string) error {
	// Determine the path to the configuration file using the following rules in order:
	//  1. Local configuration file specified using a command line argument
	//  2. If the CAGO_CONFIG_URL environment variable is set, a remote configuration file or the last downloaded file
	//  3. Local configuration file manually created by user
	var configurationFilePath string

	// 1. Local configuration file specified using a command line argument
	if configFileFlagValue != "" {
		log.Debugf("Using configuration file from command line argument: %s", configurationFilePath)

		configurationFilePath = configFileFlagValue
	} else {
		// 2. Remote configuration file specified using the CAGO_CONFIG_URL environment variable
		configurationFilePath = getRemoteConfigFile()

		if configurationFilePath != "" {
			log.Debugf("Using remote configuration file: %s", configurationFilePath)
		} else {
			// 4. Local configuration file manually created by user
			configurationFilePath = getLocalManuallyCreatedFilePath()

			if configurationFilePath != "" {
				log.Debugf("Using manually created configuration file: %s", configurationFilePath)
			} else {
				return errors.New("Unable to find configuration file")
			}
		}
	}

	// Ensure the file exists
	if _, statError := os.Stat(configurationFilePath); statError != nil {
		return errors.Wrapf(statError, "Unable to open configuration file: %s", configurationFilePath)
	}

	// Read the configuration file
	viper.SetConfigFile(configurationFilePath)
	if readInConfigError := viper.ReadInConfig(); readInConfigError != nil {
		return errors.Wrapf(readInConfigError, "Could not process configuration file: %s", configurationFilePath)
	}

	return nil
}

func getRemoteConfigFile() (filePath string) {
	remoteConfigurationFileURL, environmentVariableSet := os.LookupEnv(RemoteConfigURLEnvironmentVariable)
	if !environmentVariableSet {
		log.Debugf("Environment variable CAGO_CONFIG_URL not set")

		return ""
	}

	// Open the user's home directory
	homedirpath, homedirError := homedir.Dir()
	if homedirError != nil {
		log.Debugf("Unusual error trying to open the users home directory: %+v", homedirError)

		return ""
	}

	downloadedConfigFileLocation := filepath.Join(homedirpath, LocalCachedConfigFilePath)

	log.Debugf("Attempting to download configuration file from %s to %s", remoteConfigurationFileURL, downloadedConfigFileLocation)

	_, getError := grab.Get(downloadedConfigFileLocation, remoteConfigurationFileURL)
	if getError != nil {
		log.Debugf("Error attempting to download configuration file: %s", getError)

		// Downloading didn't work, so try returning the last cached file
		return getCachedRemoteConfigFilePath()
	}

	log.Debugf("Download succeeded!")

	return downloadedConfigFileLocation
}

func getCachedRemoteConfigFilePath() (filePath string) {
	// Open the user's home directory
	homeDirPath, homedirError := homedir.Dir()
	if homedirError != nil {
		log.Debugf("Unusual error trying to open the users home directory: %+v", homedirError)

		return ""
	}

	// This is where the file should exist
	filePath = filepath.Join(homeDirPath, LocalCachedConfigFilePath)
	log.Debugf("Checking for locally cached config file: %s", filePath)

	// Check to see if the file exists
	if _, statError := os.Stat(filePath); statError != nil {
		log.Debugf("Error checking to see if locally config cached file exists: %+v", statError)

		return ""
	}

	log.Debugf("Found locally cached configuration file: %s", filePath)

	return filePath
}

func getLocalManuallyCreatedFilePath() (filePath string) {
	// Open the user's home directory
	homeDirPath, homedirError := homedir.Dir()
	if homedirError != nil {
		log.Debugf("Unusual error trying to open the users home directory: %+v", homedirError)

		return ""
	}

	// This is where the file should exist
	filePath = filepath.Join(homeDirPath, LocalManuallyCreatedConfigFilePath)
	log.Debugf("Checking for local manually created config file: %s", filePath)

	// Check to see if the cached file exists
	if _, statError := os.Stat(filePath); statError != nil {
		log.Debugf("Error checking to see if local manually created config file exists: %+v", statError)

		return ""
	}

	log.Debugf("Found local manually created configuration file: %s", filePath)

	return filePath
}
