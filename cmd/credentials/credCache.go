package credentials // import "electric-it.io/cago/cmd/credentials"

import (
	"github.com/apex/log"
	"github.com/zalando/go-keyring"
)

// CachePassword stores the password in the OS credential store
func CachePassword(password string) {
	setError := keyring.Set("cagophilist", "password", password)
	if setError != nil {
		log.Errorf("Unable to store password in cache: %+v", setError)
		return
	}

	log.Debug("Password stored in cache")
}

// GetCachedPassword retrieves the password in the OS credential store
func GetCachedPassword() string {
	secret, getError := keyring.Get("cagophilist", "password")
	if getError != nil {
		log.Errorf("Unable to retrieve password from cache: %+v", getError)
		return ""
	}

	log.Debug("Password retrieved from cache")

	return secret
}

// CacheUsername stores the username in the OS credential store
func CacheUsername(username string) {
	setError := keyring.Set("cagophilist", "username", username)
	if setError != nil {
		log.Errorf("Unable to store username in cache: %+v", setError)
		return
	}

	log.Debug("Username stored in cache")
}

// GetCachedUsername retrieves the username in the OS credential store
func GetCachedUsername() string {
	secret, getError := keyring.Get("cagophilist", "username")
	if getError != nil {
		log.Errorf("Unable to retrieve username from cache: %+v", getError)
		return ""
	}

	log.Debug("Username retrieved from cache")

	return secret
}
